# Swiffle

![Swiffle](Swiffle.png)

This is a iOS app for playing with ideas. 

Open the Xcode project (12.4) and build and run. Add it to your Dock.

# Icons

I've used the following apps to make and export the icon sets:

- [Opacity](http://likethought.com/opacity/)
- [IconFly](https://iconfly.aperio-lux.com/)
