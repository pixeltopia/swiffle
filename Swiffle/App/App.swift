import Foundation

class App {

    private let store: Store
    private let render: Render

    private var state: State {
        get { store.state ?? .loading }
        set {
            store.state = newValue
            render(newValue)
        }
    }

    init(store: Store, render: @escaping Render) {
        self.store = store
        self.render = render
    }
}

extension App: EventSink {

    func receive(event: Event) {
        switch event {
            case .launch:           handleLaunch()
            case .foreground:       handleForeground()
            case .background:       handleBackground()
            case .increment:        handleIncrement()
            case .decrement:        handleDecrement()
            case .reset:            handleReset()
            case .menu:             handleMenu()
            case .toggle(let uuid): handleToggle(uuid: uuid)
            case .link(let link):   handleLink(link: link)
            case .back:             handleBack()
        }

        render(state)
    }

    private func handleLaunch() {
        if state == .loading {
            handleLaunchLoading()
        } else {
            handleUpdateMenu()
        }
    }

    private func handleForeground() {
        handleUpdateMenu()
    }

    private func handleBackground() {
    }

    private func handleIncrement() {
        guard let (ready, counterPage) = state.readyCounterPage
        else { return }

        state = .ready(ready.with(page: .counterPage(counterPage.increment())))
    }

    private func handleDecrement() {
        guard let (ready, counterPage) = state.readyCounterPage
        else { return }

        let nextCounter = counterPage.decrement()

        if nextCounter.value < 0 {
            state = .ready(ready.with(page: .errorPage))
        } else {
            state = .ready(ready.with(page: .counterPage(nextCounter)))
        }
    }

    private func handleReset() {
        guard let ready = state.readyError
        else { return }

        state = .ready(ready.with(page: .counterPage(CounterPage(value: 0))))
    }

    private func handleMenu() {
        guard let ready = state.ready
        else { return }

        state = .ready(ready.with(menu: ready.menu.toggled))
    }

    private func handleToggle(uuid: UUID) {
        guard let ready = state.ready
        else { return }

        state = .ready(ready.with(menu: ready.menu.toggled(uuid: uuid)))
    }

    private func handleLink(link: Link) {
        guard let ready = state.ready
        else { return }

        var newReady = ready.push(page: makePage(link: link))

        if case .menu = link.referrer {
            newReady = newReady.with(menu: ready.menu.toggled)
        }

        state = .ready(newReady)
    }

    private func makePage(link: Link) -> Page {
        link.href.scheme == "https"
            ? .webPage(WebPage(href: link.href))
            : .blankPage(BlankPage(text: link.referrer.text ?? ""))
    }

    private func handleBack() {
        guard let ready = state.ready
        else { return }

        state = .ready(ready.pop())
    }
}

private extension Referrer {

    var text: String? {
        if case .menu(let text) = self {
            return text
        } else {
            return nil
        }
    }
}

private extension App {

    func handleLaunchLoading() {
        let items = makeMenuItems()
        let menu = Menu(open: false, items: items)
        let counter = CounterPage(value: 0)
        let pages: [Page] = [.counterPage(counter)]

        let state = State.ready(Ready(age: .now, menu: menu, pages: pages))

        after(seconds: 3) {
            self.state = state
        }
    }

    func handleUpdateMenu() {
        guard let ready = state.ready, ready.age.isOlderThan(seconds: 5)
        else { return }

        state = .ready(ready.touch().with(menu: ready.menu.with(items: makeMenuItems())))
    }

    private func makeMenuItems() -> [Item] {
        [
            .menuItem(MenuItem(text: "Brown", href: "https://www.bbc.co.uk", icon: "brown")),
            .menuItem(MenuItem(text: "Emerald", href: "link://b", icon: "emerald")),
            .menuGroup(MenuGroup(text: "Group A", open: false, items: [
                .menuItem(MenuItem(text: "Lime",       href: "link://c", icon: "lime")),
                .menuItem(MenuItem(text: "Nothing",    href: "link://m")),
                .menuItem(MenuItem(text: "Orange",     href: "link://d", icon: "orange")),
                .menuItem(MenuItem(text: "Pink",       href: "link://e", icon: "pink")),
                .menuItem(MenuItem(text: "Powderpuff", href: "link://f", icon: "powderpuff")),
            ])),
            .menuGroup(MenuGroup(text: "Group B", open: false, items: [
                .menuItem(MenuItem(text: "Red",  href: "link://g", icon: "red")),
                .menuItem(MenuItem(text: "Sea",  href: "link://h", icon: "sea")),
                .menuItem(MenuItem(text: "Sky",  href: "link://i", icon: "sky")),
                .menuItem(MenuItem(text: "Tan",  href: "link://j", icon: "tan")),
                .menuItem(MenuItem(text: "Teal", href: "link://k", icon: "teal")),
            ])),
            .menuItem(MenuItem(text: "Yellow", href: "link://l", icon: "yellow")),
        ]
    }
}

private extension State {

    var readyCounterPage: (Ready, CounterPage)? {
        guard case .ready(let ready) = self,
              case .counterPage(let counterPage) = ready.pages.last
        else { return nil }

        return (ready, counterPage)
    }

    var ready: Ready? {
        guard case .ready(let ready) = self else { return nil }

        return ready
    }

    var readyError: Ready? {
        guard case .ready(let ready) = self,
              case .errorPage = ready.pages.last
        else { return nil }

        return ready
    }
}
