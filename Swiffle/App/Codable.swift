import Foundation

private enum Error: Swift.Error {
    case coding(Type)
}

extension State: Codable {

    init(from decoder: Decoder) throws {
        let container = try decoder.container()
        let type = try container.decodeType()

        switch type {
            case .empty:   self = .empty
            case .loading: self = .loading
            case .ready:   self = .ready(try container.decodeData())
            default: throw Error.coding(type)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container()

        switch self {
            case .empty:            try container.encode(type: .empty)
            case .loading:          try container.encode(type: .loading)
            case .ready(let ready): try container.encode(type: .ready, data: ready)
        }
    }
}

extension Page: Codable {

    init(from decoder: Decoder) throws {
        let container = try decoder.container()
        let type = try container.decodeType()

        switch type {
            case .counterPage: self = .counterPage(try container.decodeData())
            case .errorPage:   self = .errorPage
            case .blankPage:   self = .blankPage(try container.decodeData())
            case .webPage:     self = .webPage(try container.decodeData())
            default: throw Error.coding(type)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container()

        switch self {
            case .counterPage(let counter): try container.encode(type: .counterPage, data: counter)
            case .errorPage:                try container.encode(type: .errorPage)
            case .blankPage(let page):      try container.encode(type: .blankPage, data: page)
            case .webPage(let page):        try container.encode(type: .webPage, data: page)
        }
    }
}

extension Item: Codable {

    init(from decoder: Decoder) throws {
        let container = try decoder.container()
        let type = try container.decodeType()

        switch type {
            case .menuItem:     self = .menuItem(try container.decodeData())
            case .menuGroup:    self = .menuGroup(try container.decodeData())
            default: throw Error.coding(type)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container()

        switch self {
            case .menuItem(let item):   try container.encode(type: .menuItem, data: item)
            case .menuGroup(let group): try container.encode(type: .menuGroup, data: group)
        }
    }
}

extension Event: Codable {

    init(from decoder: Decoder) throws {
        let container = try decoder.container()
        let type = try container.decodeType()

        switch type {
            case .launch:       self = .launch
            case .foreground:   self = .foreground
            case .background:   self = .background
            case .increment:    self = .increment
            case .decrement:    self = .decrement
            case .reset:        self = .reset
            case .menu:         self = .menu
            case .back:         self = .back
            case .toggle:       self = .toggle(try container.decodeData())
            case .link:         self = .link(try container.decodeData())
            default: throw Error.coding(type)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container()

        switch self {
            case .launch:               try container.encode(type: .launch)
            case .foreground:           try container.encode(type: .foreground)
            case .background:           try container.encode(type: .background)
            case .increment:            try container.encode(type: .increment)
            case .decrement:            try container.encode(type: .decrement)
            case .reset:                try container.encode(type: .reset)
            case .menu:                 try container.encode(type: .menu)
            case .back:                 try container.encode(type: .back)
            case .toggle(let uuid):     try container.encode(type: .toggle, data: uuid)
            case .link(let referrer):   try container.encode(type: .link, data: referrer)
        }
    }
}

extension Referrer: Codable {

    init(from decoder: Decoder) throws {
        let container = try decoder.container()
        let type = try container.decodeType()

        switch type {
            case .menu:    self = .menu(try container.decodeData())
            case .webPage: self = .webPage
            default: throw Error.coding(type)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container()

        switch self {
            case .menu(let text):   try container.encode(type: .menu, data: text)
            case .webPage:          try container.encode(type: .webPage)
        }
    }
}

private enum Type: String, Codable {
    // State
    case empty
    case loading
    case ready
    // Page / Referrer
    case counterPage
    case errorPage
    case blankPage
    case webPage
    // Item
    case menuItem
    case menuGroup
    // Event / Referrer
    case launch
    case foreground
    case background
    case increment
    case decrement
    case reset
    case menu
    case back
    case toggle
    case link
}

private enum Coding: String, CodingKey {
    case type
    case data
}

private extension Encoder {
    func container() -> KeyedEncodingContainer<Coding> {
        container(keyedBy: Coding.self)
    }
}

private extension KeyedEncodingContainer where Key == Coding {

    mutating func encode(type: Type) throws {
        try encode(type, forKey: .type)
    }

    mutating func encode<T: Encodable>(type: Type, data: T) throws {
        try encode(type: type)
        try encode(data, forKey: .data)
    }
}

private extension Decoder {
    func container() throws -> KeyedDecodingContainer<Coding> {
        try container(keyedBy: Coding.self)
    }
}

private extension KeyedDecodingContainer where Key == Coding {

    func decodeType() throws -> Type {
        try decode(Type.self, forKey: .type)
    }

    func decodeData<T: Decodable>(data: T.Type = T.self) throws -> T {
        try decode(data, forKey: .data)
    }
}
