import Foundation

enum Event {
    case launch
    case foreground
    case background
    case increment
    case decrement
    case reset
    case menu
    case back
    case toggle(UUID)
    case link(Link)
}
