enum Item: Hashable {
    case menuItem(MenuItem)
    case menuGroup(MenuGroup)
}
