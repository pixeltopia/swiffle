import Foundation

struct MenuGroup: Codable, Hashable {

    let uuid: UUID
    let text: String
    let open: Bool
    let icon: String?
    let items: [Item]

    init(uuid: UUID = UUID(), text: String, open: Bool, icon: String? = nil, items: [Item]) {
        self.uuid = uuid
        self.text = text
        self.open = open
        self.icon = icon
        self.items = items
    }
}

extension MenuGroup {

    var toggled: MenuGroup {
        MenuGroup(uuid: uuid, text: text, open: !open, icon: icon, items: items)
    }
}
