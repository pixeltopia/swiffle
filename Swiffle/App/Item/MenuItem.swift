import Foundation

struct MenuItem: Codable, Hashable {

    let text: String
    let href: URL
    let icon: String?

    init(text: String, href: URL, icon: String? = nil) {
        self.text = text
        self.href = href
        self.icon = icon
    }
}
