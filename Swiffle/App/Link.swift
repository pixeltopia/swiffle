import Foundation

struct Link: Codable, Equatable {

    let href: URL
    let referrer: Referrer
}
