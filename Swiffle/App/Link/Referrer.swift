enum Referrer: Equatable {
    case menu(String)
    case webPage
}
