import Foundation

struct Menu: Codable, Equatable {

    let open: Bool
    let items: [Item]

    init(open: Bool, items: [Item] = []) {
        self.open = open
        self.items = items
    }
}

extension Menu {

    var toggled: Menu {
        Menu(open: !open, items: items)
    }

    func toggled(uuid: UUID) -> Menu {
        Menu(open: open, items: items.map {
            if case .menuGroup(let group) = $0, group.uuid == uuid {
                return .menuGroup(group.toggled)
            } else {
                return $0
            }
        })
    }

    func with(items: [Item]) -> Menu {
        Menu(open: open, items: items)
    }
}
