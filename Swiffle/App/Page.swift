import Foundation

enum Page: Equatable {
    case errorPage
    case counterPage(CounterPage)
    case blankPage(BlankPage)
    case webPage(WebPage)
}
