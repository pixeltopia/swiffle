import Foundation

struct BlankPage: Codable, Equatable {

    let text: String
}
