struct CounterPage: Codable, Equatable {

    var value: Int

    func increment() -> CounterPage {
        CounterPage(value: value + 1)
    }

    func decrement() -> CounterPage {
        CounterPage(value: value - 1)
    }
}
