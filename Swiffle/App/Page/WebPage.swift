import Foundation

struct WebPage: Codable, Equatable {

    let href: URL
}
