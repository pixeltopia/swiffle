enum State: Equatable {
    case empty
    case loading
    case ready(Ready)
}

extension State {

    var page: Page? {
        guard case .ready(let ready) = self else { return nil }
        return ready.pages.last
    }
}
