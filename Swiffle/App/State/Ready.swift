import Foundation

struct Ready: Codable, Equatable {

    let age: Date
    let menu: Menu
    let pages: [Page]

    init(age: Date, menu: Menu, pages: [Page]) {
        self.age = age
        self.menu = menu
        self.pages = pages
    }
}

extension Ready {

    func touch() -> Ready {
        Ready(age: .now, menu: menu, pages: pages)
    }

    func with(menu: Menu) -> Ready {
        Ready(age: age, menu: menu, pages: pages)
    }

    func with(page: Page) -> Ready {
        Ready(age: age, menu: menu, pages: pages.dropLast() + page)
    }

    func with(age: Date, menu: Menu) -> Ready {
        Ready(age: age, menu: menu, pages: pages)
    }

    func push(page: Page) -> Ready {
        Ready(age: age, menu: menu, pages: pages + page)
    }

    func pop() -> Ready {
        Ready(age: age, menu: menu, pages: pages.dropLast())
    }
}
