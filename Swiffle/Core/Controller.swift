import UIKit
import SwiftUI
import Combine
import SnapKit

class Controller<V>: UIViewController where V: View {

    typealias MakeEventSink = (@escaping Render) -> EventSink
    typealias MakeViewModel = () -> ViewModel
    typealias MakeView      = () -> V

    private let makeEventSink: MakeEventSink
    private let makeViewModel: MakeViewModel
    private let makeView:      MakeView

    private var viewModel:   ViewModel!
    private var cancellable: Cancellable?

    init(
        makeEventSink:  @escaping MakeEventSink,
        makeViewModel:  @escaping MakeViewModel,
        makeView:       @escaping MakeView) {
        self.makeEventSink = makeEventSink
        self.makeViewModel = makeViewModel
        self.makeView      = makeView
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = makeViewModel()
        makeController(viewModel: viewModel)

        makeCancellable()
    }

    private func makeController(viewModel: ViewModel) {
        let controller = UIHostingController(rootView: makeView().environmentObject(viewModel))
        addChild(controller)
        view.addSubview(controller.view)
        controller.view.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
        controller.didMove(toParent: self)
    }

    private func makeCancellable() {
        let eventSink = makeEventSink { [weak self] state in
            async {
                self?.viewModel.state = state
            }
        }

        cancellable = viewModel.$event
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: eventSink.receive)
    }
}

extension Controller: EventSink {

    func receive(event: Event) {
        viewModel.event = event
    }
}
