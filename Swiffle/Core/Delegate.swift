import UIKit

@main
class Delegate: UIResponder, UIApplicationDelegate {

    typealias LaunchOptions = [UIApplication.LaunchOptionsKey: Any]

    var window: UIWindow?
    var eventSink: EventSink?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: LaunchOptions?
    ) -> Bool {
        let controller = makeController()
        eventSink = controller
        window = window ?? UIWindow()
        window?.backgroundColor = .white
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        eventSink?.receive(event: .foreground)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        eventSink?.receive(event: .background)
    }

    func makeController() -> UIViewController & EventSink {
        let makeEventSink = { App(store: Store(), render: $0) }
        let makeViewModel = { ViewModel(state: .empty) }
        let makeView      = { RootView() }

        return Controller(
            makeEventSink: makeEventSink,
            makeViewModel: makeViewModel,
            makeView:      makeView)
    }
}
