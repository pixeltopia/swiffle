protocol EventSink {
    func receive(event: Event)
}
