import Foundation

extension Date {

    static var now: Date {
        Date()
    }

    func isOlderThan(seconds: TimeInterval) -> Bool {
        timeIntervalSinceNow <= seconds
    }
}

extension URL: ExpressibleByStringLiteral {

    public init(stringLiteral value: String) {
        self.init(string: "\(value)")!
    }
}

extension Array {

    static func +(lhs: Array, rhs: Element) -> Array {
        lhs + [rhs]
    }
}
