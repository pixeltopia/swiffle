import SwiftUI
import CoreGraphics

postfix operator %

extension Double {

    static postfix func % (number: Double) -> CGFloat {
        CGFloat(number / 100)
    }
}

struct Axis: OptionSet {

    let rawValue: Int

    static let horizontal = Axis(rawValue: 1)
    static let vertical   = Axis(rawValue: 2)
}

extension View {

    var flex: some View {
        flex([.horizontal, .vertical])
    }

    var bordered: some View {
        border(Color.red, width: 1)
    }

    func flex(_ axis: Axis) -> some View {
        self.frame(maxWidth: axis.contains(.horizontal) ? .infinity : nil, maxHeight: axis.contains(.vertical) ? .infinity : nil)
    }
}

func async(on queue: DispatchQueue = .main, perform work: @escaping () -> Void) {
    queue.async(execute: work)
}

func after(seconds: Int, on queue: DispatchQueue = .main, perform work: @escaping () -> Void) {
    queue.asyncAfter(deadline: .now() + .seconds(seconds), execute: work)
}
