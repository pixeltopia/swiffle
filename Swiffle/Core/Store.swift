import Foundation

@dynamicMemberLookup
class Store {

    let decoder = JSONDecoder()
    let encoder = JSONEncoder()
    let storage = UserDefaults.standard

    subscript<T: Codable>(dynamicMember member: String) -> T? {
        get {
            if let string = storage.string(forKey: key(member)), let data = string.data(using: .utf8) {
                return try? decoder.decode(T.self, from: data)
            } else {
                return nil
            }
        }
        set {
            do {
                storage.set(String(data: try encoder.encode(newValue), encoding: .utf8), forKey: key(member))
            } catch {
                print(error)
            }
        }
    }

    func key(_ member: String) -> String {
        [Bundle.main.bundleIdentifier, String(describing: self), member].compactMap({ $0 }).joined(separator: ".")
    }
}
