import Combine

class ViewModel: ObservableObject {

    @Published var state: State
    @Published var event: Event

    init(state: State, event: Event = .launch) {
        self.state = state
        self.event = event
    }
}
