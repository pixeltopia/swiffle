import SwiftUI

#if DEBUG

struct Previews: PreviewProvider {
    static var previews: some View {
        Group {
            makeRootView(ViewModel(state: .empty))
            makeRootView(ViewModel(state: .loading))
            makeRootView(ViewModel(state: .ready(Ready(age: .now, menu: makeMenu(false), pages: [makeCounterPage]))))
            makeRootView(ViewModel(state: .ready(Ready(age: .now, menu: makeMenu(true), pages: [makeCounterPage]))))
            makeRootView(ViewModel(state: .ready(Ready(age: .now, menu: makeMenu(false), pages: [.errorPage]))))
            makeRootView(ViewModel(state: .ready(Ready(age: .now, menu: makeMenu(true), pages: [.errorPage]))))
        }
    }

    static var makeCounterPage: Page {
        .counterPage(CounterPage(value: .random(in: 1...100)))
    }

    static func makeMenu(_ open: Bool) -> Swiffle.Menu {
        Swiffle.Menu(open: open)
    }

    static func makeRootView(_ model: ViewModel) -> some View {
        RootView().environmentObject(model)
    }
}

#endif
