import SwiftUI

struct BlankView: View {

    let page: BlankPage

    var body: some View {
        VStack {
            Spacer()
            Text(page.text)
            Spacer()
        }
        .flex(.horizontal)
        .background(Color.gray)
    }
}
