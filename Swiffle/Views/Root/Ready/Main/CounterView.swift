import SwiftUI

struct CounterView: View {

    @EnvironmentObject var model: ViewModel

    let page: CounterPage

    var body: some View {
        VStack {
            Spacer()
            HStack {
                button("Decrement") { model.event = .decrement }
                Text("Count: \(page.value)")
                    .padding(10)
                    .background(Color.white)
                    .accentColor(Color.black)
                button("Increment") { model.event = .increment }
            }.flex(.horizontal)
            Spacer()
        }
        .background(Color.blue)
    }

    func button(_ label: String, _ action: @escaping () -> Void) -> some View {
        Button(action: action, label: { Text(label).padding(10).background(Color.yellow) })
    }
}
