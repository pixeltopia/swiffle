import SwiftUI

struct ErrorView: View {

    @EnvironmentObject var model: ViewModel

    var body: some View {
        Button {
            model.event = .reset
        } label: {
            Text("Reset").flex
        }.background(Color.red).accentColor(Color.black)
    }
}
