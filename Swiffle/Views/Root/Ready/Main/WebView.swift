import SwiftUI
import WebKit

private let view = WKWebView()

struct WebView: UIViewRepresentable {

    @EnvironmentObject var viewModel: ViewModel

    let page: WebPage

    func makeUIView(context: Context) -> WKWebView {
        view.navigationDelegate = context.coordinator
        return view
    }

    func updateUIView(_ view: WKWebView, context: Context) {
        if view.url != page.href {
            view.load(URLRequest(url: page.href))
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(viewModel: viewModel)
    }

    class Coordinator: NSObject, WKNavigationDelegate {

        private let viewModel: ViewModel

        init(viewModel: ViewModel) {
            self.viewModel = viewModel
        }

        func webView(
            _ webView: WKWebView,
            decidePolicyFor navigationAction: WKNavigationAction,
            decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

            guard let href = navigationAction.request.url,
                  navigationAction.sourceFrame.isMainFrame,
                  navigationAction.navigationType == .linkActivated
            else { return decisionHandler(.allow) }

            viewModel.event = .link(Link(href: href, referrer: .webPage))
            decisionHandler(.cancel)
        }
    }
}
