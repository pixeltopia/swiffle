import SwiftUI

struct MainOverlayView: View {

    @EnvironmentObject var model: ViewModel

    let label: String

    var body: some View {
        Button(action: { model.event = .menu }) {
            Color.white.opacity(0)
        }.flex.accessibility(label: Text(label))
    }
}
