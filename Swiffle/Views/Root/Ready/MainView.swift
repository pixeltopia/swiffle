import SwiftUI

struct MainView: View {

    @EnvironmentObject var model: ViewModel

    var body: some View {
        NavigationView {
            view.navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        Label("Swiffle", systemImage: "swift")
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action: { model.event = .menu }) {
                            Text("Menu")
                        }
                    }
                    ToolbarItem(placement: .navigationBarLeading) {
                        if model.state.showsBack {
                            Button(action: { model.event = .back }) {
                                Text("Back")
                            }
                        }
                    }
                }
                .animation(nil)
        }
    }

    var view: some View {
        switch model.state.page {
            case .counterPage(let page):
                return AnyView(CounterView(page: page))
            case .errorPage:
                return AnyView(ErrorView())
            case .blankPage(let page):
                return AnyView(BlankView(page: page))
            case .webPage(let page):
                return AnyView(WebView(page: page))
            case .none:
                return AnyView(Color.black)
        }
    }
}

private extension State {

    var showsBack: Bool {
        guard case .ready(let ready) = self else { return false }
        return ready.pages.count > 1
    }
}
