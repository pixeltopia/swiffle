import SwiftUI

struct MenuGroupView: View {

    @EnvironmentObject var model: ViewModel

    let group: MenuGroup

    var body: some View {
        HStack(alignment: .center) {
            if let icon = group.icon {
                Image(icon)
                    .resizable()
                    .frame(width: 36, height: 36)
            }

            Text(group.text)
                .background(Color.clear)
                .foregroundColor(Color.white)
                .font(.body)
                .frame(maxWidth: .infinity, alignment: .leading)

            Image("chevron")
                .resizable()
                .frame(width: 18, height: 18)
                .rotationEffect(.degrees(group.open ? -180 : 0))
        }
        .padding(5)
        .onTapGesture {
            model.event = .toggle(group.uuid)
        }
    }
}
