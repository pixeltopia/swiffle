import SwiftUI

struct MenuItemView: View {

    @EnvironmentObject var model: ViewModel

    let item: MenuItem

    var body: some View {
        HStack(alignment: .center) {
            if let icon = item.icon {
                Image(icon)
                    .resizable()
                    .frame(width: 36, height: 36)
            } else {
                Color.clear
                    .frame(width: 36, height: 36)
            }

            Text(item.text)
                .background(Color.clear)
                .foregroundColor(Color.white)
                .font(.body)
                .frame(maxWidth: .infinity, alignment: .leading)
        }
        .padding(5)
        .onTapGesture {
            model.event = .link(Link(href: item.href, referrer: .menu(item.text)))
        }
    }
}
