import SwiftUI

struct MenuView: View {

    let menu: Menu

    var body: some View {
        ScrollView(showsIndicators: false) {
            LazyVStack {
                ForEach(menu.expanded, id: \.self) { item in
                    switch item {
                        case .menuItem(let item):
                            MenuItemView(item: item)
                        case .menuGroup(let group):
                            MenuGroupView(group: group)
                    }
                }
            }
        }
        .background(Color.black)
        .animation(.easeInOut)
    }
}

private extension Menu {

    var expanded: [Item] {
        items.map { (item: Item) -> [Item] in
            switch item {
                case .menuItem:             return [item]
                case .menuGroup(let group): return group.open ? [item] + group.items : [item]
            }
        }.flatMap { $0 }
    }
}
