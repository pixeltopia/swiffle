import SwiftUI

struct SideView: View {

    @EnvironmentObject var model: ViewModel

    var body: some View {
        if let menu = model.state.menu {
            MenuView(menu: menu)
        } else {
            EmptyView()
        }
    }
}

private extension State {

    var menu: Menu? {
        guard case .ready(let ready) = self else { return nil }
        return ready.menu
    }
}
