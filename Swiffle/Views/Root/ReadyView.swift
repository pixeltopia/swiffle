import SwiftUI

struct ReadyView: View {

    @EnvironmentObject var model: ViewModel

    @SwiftUI.State var animating = false

    private var isMenuOpen: Bool { model.state.isMenuOpen }

    var body: some View {
        Group {
            GeometryReader { geometry in
                let width  = geometry.size.width
                let offset = CGFloat(180) // width * 70%
                SideView().frame(width: offset).offset(x: width - offset)
                Group {
                    MainView().accessibility(hidden: isMenuOpen)
                    if isMenuOpen { MainOverlayView(label: "Close") }
                }
                .offset(x: isMenuOpen ? -offset : .zero)
                .animation(animating ? Animation.easeInOut.speed(1.5) : nil)
            }.onAppear {
                animating = true
            }
        }
    }
}

private extension State {

    var isMenuOpen: Bool {
        guard case .ready(let ready) = self else { return false }
        return ready.menu.open
    }
}
