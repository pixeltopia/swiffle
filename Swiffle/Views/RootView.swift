import SwiftUI

struct RootView: View {

    @EnvironmentObject var model: ViewModel

    var body: some View {
        Group {
            switch model.state {
                case .empty:
                    EmptyView()
                case .loading:
                    LoadingView()
                case .ready:
                    ReadyView()
            }
        }
    }
}
