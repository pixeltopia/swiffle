#!/usr/bin/env ruby

require 'rexml/document'
require 'base64'
require 'json'
require 'tempfile'

include REXML

file = `xcrun simctl get_app_container booted uk.co.pixeltopia.Swiffle data`.chomp + "/Library/Preferences/uk.co.pixeltopia.Swiffle.plist"
xml  = `plutil -convert xml1 #{file} -o -`
dict = {}

puts
puts file
puts

Document.new(xml).elements.each("plist/dict/key") do |element|
    name = element.text
    case element.next_element.name
    when 'true'
        value = true
    when 'false'
        value = false
    when 'integer'
        value = element.next_element.text.to_i
    when 'data'
        decoded = Base64.decode64(element.next_element.text)
        if decoded.start_with? 'bplist00'
            temp = Tempfile.new('user-defaults-plist')
            temp << decoded
            temp.flush
            temp.close
            path = temp.path
            value = `plutil -convert xml1 #{path} -o -`
        else
            begin
                value = JSON.parse(decoded)
            rescue
                fail "unsupported data = #{decoded}"
            end
        end
    when 'string'
        value = element.next_element.text
        begin
            value = JSON.parse(value)
        rescue
            fail "unsupported data = #{value}"
        end
    else
        value = nil
    end
    dict[name] = value
end

puts JSON.pretty_generate(dict)
